$(function() {
  var socket = io.connect("http://localhost:3000");

  var message = $("#message");
  var contact = $(".contact");
  var profile = $('#profileUser');
  var send_message = $("#send_message");
  var contentProfile = $('#contentProfile');
  var chatroom = $("#chatroom");
  var feedback = $("#feedback");

  contact.click(() => {
    contentProfile.css('display', 'block')
    socket.emit("change_username", { username: profile.data('name'), img: profile.data('img') });
  })

  send_message.click(() => {
    socket.emit("new_message", {
      message: message.val(),
      className: alertClass
    });
  });
  var min = 1;
  var max = 6;
  var random = Math.floor(Math.random() * (max - min)) + min;

  // Устаналиваем класс в переменную в зависимости от случайного числа
  // Эти классы взяты из Bootstrap стилей
  var alertClass;
  switch (random) {
    case 1:
      alertClass = "secondary";
      break;
    case 2:
      alertClass = "danger";
      break;
    case 3:
      alertClass = "success";
      break;
    case 4:
      alertClass = "warning";
      break;
    case 5:
      alertClass = "info";
      break;
    case 6:
      alertClass = "light";
      break;
  }
  

  socket.on("add_mess", data => {
    console.log(data, 'message')
    feedback.html("");
    message.val("");
    var classNames="replies";
    if(profile.data('name') !== data.username) {
      classNames="sent";
    }
    chatroom.append(
      `<li class="${classNames}">
        <img src="${data.img}" alt="">
        <p>${data.message}</p>
      </li>`
    );
  });

  message.bind("keypress", () => {
    socket.emit("typing");
  });

  socket.on("typing", data => {
    feedback.html(
      "<p><i>" + data.username + " печатает сообщение..." + "</i></p>"
    );
  });
});
