const express = require("express");
const app = express();

app.set('view engine', 'ejs');

app.use(express.static("public"));

app.get('/', (req, res) => {
	res.render('index')
})

app.get('/new', (req, res) => {
	res.render('newUser')
})
server = app.listen("3000", () => console.log("Server is running..."));

const io = require("socket.io")(server);

io.on('connection', (socket) => {
	console.log('New user connected')

    socket.username = "Anonymous"
    socket.img = "none"

    socket.on('change_username', (data) => {
        console.log('change_username', data)
        socket.username = data.username
        socket.img = data.img
    })

    socket.on('new_message', (data) => {
        io.sockets.emit('add_mess', {message : data.message, username : socket.username, img : socket.img });
    })

    socket.on('typing', (data) => {
    	socket.broadcast.emit('typing', {username : socket.username})
    })
})
